package GUI;

import CommandCreator.ExitCommandCaller;
import CommandCreator.UserFunctionCaller;
import entity.TransportObject;
import entity.User;

import javax.swing.*;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;

public class LoginWindow {
    private JFrame frame;


    public static PrintWriter out;
    public static BufferedReader input;


    public static void main(String args[]) {

        EventQueue.invokeLater(new Runnable() {
            public void run(){
                try {
                    LoginWindow loginWindow = new LoginWindow();
                    loginWindow.frame.setVisible(true);
                } catch (Exception e){
                    e.printStackTrace();
                }
            }
        });
    }


    public LoginWindow()  throws IOException {
        Socket s = new Socket("localhost", 9090);
        input = new BufferedReader(new InputStreamReader(s.getInputStream()));
        out = new PrintWriter(s.getOutputStream(), true);
        initialize(input, out, s);
    }

    public void initialize(final BufferedReader input, final PrintWriter out, final Socket s) throws IOException {


        frame = new JFrame("Login Window");
        frame.setBounds(100, 100, 800, 640);
        frame.getContentPane().setLayout(null);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setVisible(true);

        final JPanel panel = new JPanel();
        panel.setVisible(true);
        frame.add(panel);
        final JTextField userName = new JTextField();
        userName.setVisible(true);
        userName.setBounds(280, 120, 200, 30);
        frame.add(userName);
        final JPasswordField passwordField = new JPasswordField();
        passwordField.setVisible(true);
        passwordField.setBounds(280, 160, 200, 30);
        frame.add(passwordField);

        final AdminMenu adminMenu = new AdminMenu();
        final WriterMenu writerMenu = new WriterMenu();
        final ReaderMenu readerMenu = new ReaderMenu();

        final UserFunctionCaller userFunctionCaller = new UserFunctionCaller();
        final ExitCommandCaller exitCommandCaller = new ExitCommandCaller();

        final JButton loginButton = new JButton("Login");
        loginButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent arg0) {
                try {

                    User user = userFunctionCaller.getUserByName(userName.getText());
                    if (user.getPassword().equals(passwordField.getText())){
                        if (!user.getIsAdmin()){
                            writerMenu.initialize(input, out, s, user);
                        } else if (user.getIsAdmin()){
                            adminMenu.initialize(input, out, s);
                        }
                    }
                } catch (Exception e){
                    e.printStackTrace();
                }
            }
        });
        loginButton.setBounds(280, 200, 200, 40);
        frame.getContentPane().add(loginButton);

        final JButton readerButton = new JButton("Enter as reader");
        readerButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent arg0) {
                try {
                    readerMenu.initialize(input, out, s);
                } catch (Exception e){
                    e.printStackTrace();
                }
            }
        });
        readerButton.setBounds(280, 250, 200, 40);
        frame.getContentPane().add(readerButton);

        final JButton exitButton = new JButton("Exit");
        exitButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent arg0) {
                exitCommandCaller.ExitCommand();
                frame.dispose();
            }
        });
        exitButton.setBounds(280, 400, 200, 40);
        frame.getContentPane().add(exitButton);
    }

    private void createUIComponents() {
        // TODO: place custom component creation code here
    }
}
