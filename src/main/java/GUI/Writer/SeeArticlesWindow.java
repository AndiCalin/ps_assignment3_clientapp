package GUI.Writer;

import CommandCreator.ArticleFunctionCaller;
import CommandCreator.UserFunctionCaller;
import entity.Article;
import entity.User;
import util.TableGenerator;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.lang.reflect.Field;
import java.net.Socket;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.List;

public class SeeArticlesWindow{
    private JFrame frame;
    private TextArea textArea;
    private JTextArea articleBody;
    private JTextArea textAreaTitle;
    private JTextArea textAreaAbstract;
    private JLabel articleBodyLabel;
    private JLabel articleTitleLabel;
    private JLabel articleAbstractLabel;
    private JTable table;

    public SeeArticlesWindow() {

    }


    public void removeRows(DefaultTableModel tableModel) {
        for (int i = table.getRowCount() - 1; i >= 0; i--) {
            tableModel.removeRow(i);
        }
    }

    public void initialize(final BufferedReader input, final PrintWriter out, final Socket s, final User user){
        frame = new JFrame("Add Article Menu");
        frame.setBounds(100, 100, 800, 640);
        frame.getContentPane().setLayout(null);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setVisible(true);

        String columns[] = {};
        final DefaultTableModel tableModel = new DefaultTableModel(columns, 0);
        textArea = new TextArea();
        textArea.setBounds(10, 10, 780, 60);
        frame.getContentPane().add(textArea);
        articleBody = new JTextArea();
        articleBody.setLineWrap(true);
        articleBody.setBounds(50, 270, 500, 220);
        frame.getContentPane().add(articleBody);
        articleBodyLabel = new JLabel("Article Body");
        articleBodyLabel.setBounds(50, 240, 200, 20);
        frame.getContentPane().add(articleBodyLabel);
        textAreaTitle = new JTextArea();
        textAreaTitle.setBounds(10, 80, 500, 50);
        frame.getContentPane().add(textAreaTitle);
        articleTitleLabel = new JLabel("Article Title");
        articleTitleLabel.setBounds(520, 80, 200, 50);
        frame.getContentPane().add(articleTitleLabel);
        textAreaAbstract = new JTextArea();
        textAreaAbstract.setBounds(10, 160, 500, 50);
        frame.getContentPane().add(textAreaAbstract);
        articleAbstractLabel = new JLabel("Article Abstract");
        articleAbstractLabel.setBounds(520, 160, 200, 50);
        frame.getContentPane().add(articleAbstractLabel);
        textAreaAbstract.setVisible(false);
        textAreaTitle.setVisible(false);
        articleAbstractLabel.setVisible(false);
        articleTitleLabel.setVisible(false);
        articleBodyLabel.setVisible(false);
        table = new JTable(tableModel);
        table.setBounds(10, 80, 780, 180);
        frame.getContentPane().add(table);


        UserFunctionCaller userFunctionCaller = new UserFunctionCaller();
        final ArticleFunctionCaller articleFunctionCaller = new ArticleFunctionCaller();
        final AddArticleWindow addArticleWindow = new AddArticleWindow();
        final UpdateArticleWindow updateArticleWindow = new UpdateArticleWindow();
        final SeeArticlesWindow seeArticlesWindow = new SeeArticlesWindow();

        final JButton allArticlesButton = new JButton("See All Articles");
        allArticlesButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent arg0) {
                try {
                    removeRows(tableModel);
                    articleBody.setText("");
                    table.setVisible(true);
                    textAreaTitle.setVisible(false);
                    textAreaAbstract.setVisible(false);
                    textAreaTitle.setVisible(false);
                    articleAbstractLabel.setVisible(false);
                    articleTitleLabel.setVisible(false);
                    articleBodyLabel.setVisible(false);
                    List<Article> listResult = articleFunctionCaller.getAllArticlesForUser(user);
                    TableGenerator.generateTable(listResult, tableModel);
                    if(listResult.size() == 0){
                        textArea.setText("No articles to show!");
                    }
                } catch (Exception e){
                    e.printStackTrace();
                }
            }
        });
        allArticlesButton.setBounds(600, 300, 150, 25);
        frame.getContentPane().add(allArticlesButton);

        final JButton articleByIdButton = new JButton("Get Article By Id");
        articleByIdButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent arg0) {
                try {
                    removeRows(tableModel);
                    table.setVisible(false);
                    textAreaTitle.setVisible(true);
                    textAreaAbstract.setVisible(true);
                    textAreaTitle.setVisible(true);
                    articleAbstractLabel.setVisible(true);
                    articleTitleLabel.setVisible(true);
                    articleBodyLabel.setVisible(true);
                    int articleId = Integer.parseInt(JOptionPane.showInputDialog(frame, "Insert article id:"));
                    Article article = articleFunctionCaller.getArticleById(articleId);
                    if (article == null || article.getAuthorId() != user.getId()) {
                        textArea.setText("User has no articles to show for that id!");
                        return;
                    }
                    if (article.getAuthorId() == user.getId()) {
                        textAreaTitle.setText(article.getTitle());
                        textAreaAbstract.setText(article.getAbstractOfArticle());
                        articleBody.setText(article.getBody());
                    }
                } catch (RuntimeException e){
                    textArea.setText(e.getMessage());
                } catch (Exception e){
                    e.printStackTrace();
                }
            }
        });
        articleByIdButton.setBounds(600, 330, 150, 25);
        frame.getContentPane().add(articleByIdButton);

        final JButton articleByNameButton = new JButton("Get Article By Title");
        articleByNameButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent arg0) {
                try {
                    removeRows(tableModel);
                    table.setVisible(false);
                    textAreaTitle.setVisible(true);
                    textAreaAbstract.setVisible(true);
                    textAreaTitle.setVisible(true);
                    articleAbstractLabel.setVisible(true);
                    articleTitleLabel.setVisible(true);
                    articleBodyLabel.setVisible(true);
                    String title = JOptionPane.showInputDialog(frame, "Insert article title:");
                    Article article = articleFunctionCaller.getArticleByTitle(title);
                    if (article == null || article.getAuthorId() != user.getId()) {
                        textArea.setText("User has no articles to show for that title!");
                        return;
                    }
                    if (article.getAuthorId() == user.getId()) {
                        textAreaTitle.setText(article.getTitle());
                        textAreaAbstract.setText(article.getAbstractOfArticle());
                        articleBody.setText(article.getBody());
                    }
                } catch (RuntimeException e){
                    textArea.setText(e.getMessage());
                } catch (Exception e){
                    e.printStackTrace();
                }
            }
        });
        articleByNameButton.setBounds(600, 360, 150, 25);
        frame.getContentPane().add(articleByNameButton);


        final JButton backButton = new JButton("Back");
        backButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent arg0) {
                frame.dispose();
            }
        });
        backButton.setBounds(600, 480, 150, 25);
        frame.getContentPane().add(backButton);
    }

    private void createUIComponents() {
        // TODO: place custom component creation code here
    }
}