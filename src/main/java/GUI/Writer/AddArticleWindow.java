package GUI.Writer;

import CommandCreator.ArticleFunctionCaller;
import CommandCreator.UserFunctionCaller;
import entity.Article;
import entity.User;
import util.TableGenerator;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.List;

public class AddArticleWindow {

    private JFrame frame;
    private TextArea textArea;
    private JTextArea articleBody;
    private JTextArea textAreaTitle;
    private JTextArea textAreaAbstract;
    private JLabel articleBodyLabel;
    private JLabel articleTitleLabel;
    private JLabel articleAbstractLabel;


    public AddArticleWindow() {

    }

    public void initialize(final BufferedReader input, final PrintWriter out, final Socket s, final User user){
        frame = new JFrame("Add Article Menu");
        frame.setBounds(100, 100, 800, 640);
        frame.getContentPane().setLayout(null);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setVisible(true);

        textArea = new TextArea();
        textArea.setBounds(10, 10, 780, 60);
        frame.getContentPane().add(textArea);
        articleBody = new JTextArea();
        articleBody.setLineWrap(true);
        articleBody.setBounds(50, 270, 500, 220);
        frame.getContentPane().add(articleBody);
        articleBodyLabel = new JLabel("Article Body");
        articleBodyLabel.setBounds(50, 240, 200, 20);
        frame.getContentPane().add(articleBodyLabel);
        textAreaTitle = new JTextArea();
        textAreaTitle.setBounds(10, 80, 500, 50);
        frame.getContentPane().add(textAreaTitle);
        articleTitleLabel = new JLabel("Article Title");
        articleTitleLabel.setBounds(520, 80, 200, 50);
        frame.getContentPane().add(articleTitleLabel);
        textAreaAbstract = new JTextArea();
        textAreaAbstract.setBounds(10, 160, 500, 50);
        frame.getContentPane().add(textAreaAbstract);
        articleAbstractLabel = new JLabel("Article Abstract");
        articleAbstractLabel.setBounds(520, 160, 200, 50);
        frame.getContentPane().add(articleAbstractLabel);
        textAreaAbstract.setVisible(true);
        textAreaTitle.setVisible(true);
        articleAbstractLabel.setVisible(true);
        articleTitleLabel.setVisible(true);
        articleBodyLabel.setVisible(true);

        UserFunctionCaller userFunctionCaller = new UserFunctionCaller();
        final ArticleFunctionCaller articleFunctionCaller = new ArticleFunctionCaller();


        final JButton addArticleButton = new JButton("Add Article");
        addArticleButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent arg0) {
                try {
                    String title = textAreaTitle.getText();
                    String abstractOfTheArticle = textAreaAbstract.getText();
                    String articleText = articleBody.getText();
                    if (title.length() < 1 || abstractOfTheArticle.length() < 1 || articleText.length() < 1){
                        textArea.setText("Please write all fields!");
                        return;
                    }
                    Article article = new Article();
                    article.setTitle(title);
                    article.setAuthorId(user.getId());
                    article.setBody(articleText);
                    article.setAbstractOfArticle(abstractOfTheArticle);
                    articleFunctionCaller.insertArticle(article);
                    textArea.setText("Article added!");
                    frame.dispose();
                } catch (RuntimeException e){
                    textArea.setText(e.getMessage());
                } catch (Exception e){
                    e.printStackTrace();
                }
            }
        });
        addArticleButton.setBounds(600, 280, 150, 25);
        frame.getContentPane().add(addArticleButton);


        final JButton backButton = new JButton("Back");
        backButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent arg0) {
                frame.dispose();
            }
        });
        backButton.setBounds(600, 480, 150, 25);
        frame.getContentPane().add(backButton);
    }

    private void createUIComponents() {
        // TODO: place custom component creation code here
    }

}
