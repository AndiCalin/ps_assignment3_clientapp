package GUI.Writer;

import CommandCreator.ArticleFunctionCaller;
import CommandCreator.UserFunctionCaller;
import entity.Article;
import entity.User;
import util.TableGenerator;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.lang.reflect.Field;
import java.net.Socket;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.List;

public class DeleteArticleWindow{
    private JFrame frame;
    private JTable table;
    private TextArea textArea;


    public DeleteArticleWindow() {

    }

    public void removeRows(DefaultTableModel tableModel) {
        for (int i = table.getRowCount() - 1; i >= 0; i--) {
            tableModel.removeRow(i);
        }
    }

    public void initialize(final BufferedReader input, final PrintWriter out, final Socket s, final User user) throws IllegalAccessException {
        frame = new JFrame("Delete Articles Menu");
        frame.setBounds(100, 100, 800, 640);
        frame.getContentPane().setLayout(null);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setVisible(true);

        textArea = new TextArea();
        textArea.setBounds(10, 10, 780, 60);
        frame.getContentPane().add(textArea);
        String columns[] = {};
        final DefaultTableModel tableModel = new DefaultTableModel(columns, 0);
        table = new JTable(tableModel);
        table.setBounds(10, 80, 780, 180);
        frame.getContentPane().add(table);

        UserFunctionCaller userFunctionCaller = new UserFunctionCaller();
        final ArticleFunctionCaller articleFunctionCaller = new ArticleFunctionCaller();

        removeRows(tableModel);
        final List<Article> listResult = articleFunctionCaller.getAllArticlesForUser(user);
        TableGenerator.generateTable(listResult, tableModel);
        if(listResult.size() == 0){
            textArea.setText("No articles to show!");
        }

        table.addMouseListener(new java.awt.event.MouseAdapter() {
            @Override
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                int row = table.rowAtPoint(evt.getPoint()) - 1;
                int col = table.columnAtPoint(evt.getPoint());
                if (row >= 0 && col >= 0) {
                    int reply = JOptionPane.showConfirmDialog(frame, "Are you sure you want to delete this article?", "Confirmation of Deletion",
                            JOptionPane.YES_NO_OPTION);
                    if (reply == JOptionPane.YES_OPTION) {
                        articleFunctionCaller.deleteArticle(listResult.get(row).getId());
                        try {
                            removeRows(tableModel);
                            TableGenerator.generateTable(articleFunctionCaller.getAllArticlesForUser(user), tableModel);
                        } catch (IllegalAccessException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
        });


        final JButton backButton = new JButton("Back");
        backButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent arg0) {
                frame.dispose();
            }
        });
        backButton.setBounds(100, 480, 150, 25);
        frame.getContentPane().add(backButton);
    }

    private void createUIComponents() {
        // TODO: place custom component creation code here
    }
}