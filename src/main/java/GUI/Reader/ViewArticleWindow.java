package GUI.Reader;

import CommandCreator.ArticleFunctionCaller;
import CommandCreator.UserFunctionCaller;
import entity.Article;
import entity.User;
import util.TableGenerator;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.List;

public class ViewArticleWindow {

    private JFrame frame;
    private JTextArea textArea;

    public ViewArticleWindow() {

    }

    public void initialize(final BufferedReader input, final PrintWriter out, final Socket s, final User user, final Article article) throws IllegalAccessException {
        frame = new JFrame("View Article");
        frame.setBounds(100, 100, 800, 640);
        frame.getContentPane().setLayout(null);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setVisible(true);

        textArea = new JTextArea();
        textArea.setBounds(100, 100, 600, 400);
        frame.getContentPane().add(textArea);

        textArea.setText("\""+ article.getTitle() + "\" by " + user.getUserName() +
        "\n\n\n" + article.getBody());



        final JButton backButton = new JButton("Back");
        backButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent arg0) {
                frame.dispose();
            }
        });
        backButton.setBounds(600, 750, 150, 25);
        frame.getContentPane().add(backButton);
    }

    private void createUIComponents() {
        // TODO: place custom component creation code here
    }

}