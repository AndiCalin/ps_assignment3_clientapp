package GUI;

import CommandCreator.ArticleFunctionCaller;
import CommandCreator.UserFunctionCaller;
import GUI.Writer.AddArticleWindow;
import GUI.Writer.DeleteArticleWindow;
import GUI.Writer.SeeArticlesWindow;
import GUI.Writer.UpdateArticleWindow;
import entity.Article;
import entity.User;
import util.TableGenerator;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.lang.reflect.Field;
import java.net.Socket;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.List;

public class WriterMenu{
    private JFrame frame;
    private JTable table;
    private TextArea textArea;

    public WriterMenu() {

    }

    public void removeRows(DefaultTableModel tableModel) {
        for (int i = table.getRowCount() - 1; i >= 0; i--) {
            tableModel.removeRow(i);
        }
    }

    public void refresh(DefaultTableModel tableModel, User user) throws IllegalAccessException {
        ArticleFunctionCaller articleFunctionCaller = new ArticleFunctionCaller();
        List<Article> results = articleFunctionCaller.getAllArticlesForUser(user);
        removeRows(tableModel);
        TableGenerator.generateTable(results, tableModel);
    }



    public void initialize(final BufferedReader input, final PrintWriter out, final Socket s, final User user) throws IllegalAccessException {
        frame = new JFrame("Writer Menu");
        frame.setBounds(100, 100, 800, 640);
        frame.getContentPane().setLayout(null);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setVisible(true);

        textArea = new TextArea();
        textArea.setBounds(10, 10, 780, 60);
        frame.getContentPane().add(textArea);
        String columns[] = {};
        final DefaultTableModel tableModel = new DefaultTableModel(columns, 0);
        table = new JTable(tableModel);
        table.setBounds(10, 80, 780, 180);
        frame.getContentPane().add(table);

        UserFunctionCaller userFunctionCaller = new UserFunctionCaller();
        final ArticleFunctionCaller articleFunctionCaller = new ArticleFunctionCaller();
        final AddArticleWindow addArticleWindow = new AddArticleWindow();
        final UpdateArticleWindow updateArticleWindow = new UpdateArticleWindow();
        final SeeArticlesWindow seeArticlesWindow = new SeeArticlesWindow();
        final DeleteArticleWindow deleteArticleWindow = new DeleteArticleWindow();

        removeRows(tableModel);
        final List<Article> listResult = articleFunctionCaller.getAllArticlesForUser(user);
        TableGenerator.generateTable(listResult, tableModel);
        if(listResult.size() == 0){
            textArea.setText("No articles to show!");
        }

        table.addMouseListener(new java.awt.event.MouseAdapter() {
            @Override
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                int row = table.rowAtPoint(evt.getPoint()) - 1;
                int col = table.columnAtPoint(evt.getPoint());
                if (row >= 0 && col >= 0) {
                    int reply = JOptionPane.showConfirmDialog(frame, "Are you sure you want to update this article?", "Confirmation of Deletion",
                            JOptionPane.YES_NO_OPTION);
                    if (reply == JOptionPane.YES_OPTION) {
                        final List<Article> listForManipulation = new ArrayList<Article>();
                        listForManipulation.addAll(articleFunctionCaller.getAllArticlesForUser(user));
                        Article article = listForManipulation.get(row);
                        try {
                            updateArticleWindow.initialize(input, out, s, user, article);
                            removeRows(tableModel);
                            TableGenerator.generateTable(articleFunctionCaller.getAllArticlesForUser(user), tableModel);
                        } catch (IllegalAccessException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
        });

        final JButton allArticlesButton = new JButton("See All Articles");
        allArticlesButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent arg0) {
                try {
                    removeRows(tableModel);
                    List<Article> listResult = articleFunctionCaller.getAllArticlesForUser(user);
                    TableGenerator.generateTable(listResult, tableModel);
                    if(listResult.size() == 0){
                        textArea.setText("No articles to show!");
                    }
                } catch (Exception e){
                    e.printStackTrace();
                }
            }
        });
        allArticlesButton.setBounds(260, 300, 150, 25);
        frame.getContentPane().add(allArticlesButton);

        final JButton addArticleButton = new JButton("Add An Article");
        addArticleButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent arg0) {
                addArticleWindow.initialize(input, out, s, user);
            }
        });
        addArticleButton.setBounds(260, 330, 150, 25);
        frame.getContentPane().add(addArticleButton);

        final JButton deleteArticleButton = new JButton("Delete Article");
        deleteArticleButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent arg0) {
                try {
                    deleteArticleWindow.initialize(input, out, s, user);
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                }
            }
        });
        deleteArticleButton.setBounds(260, 360, 150, 25);
        frame.getContentPane().add(deleteArticleButton);


        final JButton backButton = new JButton("Back");
        backButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent arg0) {
                frame.dispose();
            }
        });
        backButton.setBounds(260, 480, 150, 25);
        frame.getContentPane().add(backButton);
    }

    private void createUIComponents() {
        // TODO: place custom component creation code here
    }
}