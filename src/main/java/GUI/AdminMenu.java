package GUI;

import CommandCreator.ArticleFunctionCaller;
import CommandCreator.UserFunctionCaller;
import entity.Article;
import entity.User;
import util.TableGenerator;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.lang.reflect.Field;
import java.net.Socket;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.List;

public class AdminMenu{
    private JFrame frame;
    private JTable table;
    private TextArea textArea;
    private TextArea articleBody;


    public AdminMenu() {

    }

    public void removeRows(DefaultTableModel tableModel) {
        for (int i = table.getRowCount() - 1; i >= 0; i--) {
            tableModel.removeRow(i);
        }
    }

    public void initialize(final BufferedReader input, final PrintWriter out, final Socket s){
        frame = new JFrame("Admin Menu");
        frame.setBounds(100, 100, 800, 640);
        frame.getContentPane().setLayout(null);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setVisible(true);

        textArea = new TextArea();
        textArea.setBounds(10, 10, 780, 60);
        frame.getContentPane().add(textArea);
        String columns[] = {};
        final DefaultTableModel tableModel = new DefaultTableModel(columns, 0);
        table = new JTable(tableModel);
        table.setBounds(10, 80, 780, 180);
        frame.getContentPane().add(table);

        final UserFunctionCaller userFunctionCaller = new UserFunctionCaller();


        final JButton allUsersButton = new JButton("See All Users");
        allUsersButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent arg0) {
                try {
                    removeRows(tableModel);
                    List<User> listResult = userFunctionCaller.getAllUsers();
                    TableGenerator.generateTable(listResult, tableModel);
                    if(listResult.size() == 0){
                        textArea.setText("No users to show!");
                    }
                } catch (Exception e){
                    e.printStackTrace();
                }
            }
        });
        allUsersButton.setBounds(350, 300, 150, 25);
        frame.getContentPane().add(allUsersButton);

        final JButton userByNameButton = new JButton("Get User By Name");
        userByNameButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent arg0) {
                try {
                    removeRows(tableModel);
                    String userName = JOptionPane.showInputDialog(frame, "Insert user name:");
                    User user = userFunctionCaller.getUserByName(userName);
                    TableGenerator.generateTable(user , tableModel);
                    if (user == null) {
                        textArea.setText("No user to show for that name!");
                    }
                } catch (RuntimeException e){
                    textArea.setText(e.getMessage());
                } catch (Exception e){
                    e.printStackTrace();
                }
            }
        });
        userByNameButton.setBounds(350, 330, 150, 25);
        frame.getContentPane().add(userByNameButton);

        final JButton addUserButton = new JButton("Add Article");
        addUserButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent arg0) {
                try {
                    String name = JOptionPane.showInputDialog(frame, "Insert user name:");
                    String password = JOptionPane.showInputDialog(frame, "Insert user password:");
                    User user = new User();
                    user.setUserName(name);
                    user.setPassword(password);
                    user.setIsAdmin(false);
                    userFunctionCaller.insertUser(user);
                    textArea.setText("User added!");
                } catch (RuntimeException e){
                    textArea.setText(e.getMessage());
                } catch (Exception e){
                    e.printStackTrace();
                }
            }
        });
        addUserButton.setBounds(350, 360, 150, 25);
        frame.getContentPane().add(addUserButton);

        final JButton updateUserButton = new JButton("Update User");
        updateUserButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent arg0) {
                try {
                    int userId = Integer.parseInt(JOptionPane.showInputDialog(frame, "Insert user id:"));
                    String name = JOptionPane.showInputDialog(frame, "Insert user name:");
                    String password = JOptionPane.showInputDialog(frame, "Insert user password:");
                    User user = new User();
                    user.setId(userId);
                    user.setUserName(name);
                    user.setPassword(password);
                    user.setIsAdmin(false);
                    userFunctionCaller.updateUser(user);
                    textArea.setText("User updated!");
                } catch (RuntimeException e){
                    textArea.setText(e.getMessage());
                } catch (Exception e){
                    e.printStackTrace();
                }
            }
        });
        updateUserButton.setBounds(350, 390, 150, 25);
        frame.getContentPane().add(updateUserButton);

        final JButton deleteUserButton = new JButton("Delete User");
        deleteUserButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent arg0) {
                try {
                    int userId = Integer.parseInt(JOptionPane.showInputDialog(frame, "Insert user id:"));
                    userFunctionCaller.deleteUser(userId);
                    textArea.setText("User deleted!");
                } catch (RuntimeException e){
                    textArea.setText(e.getMessage());
                } catch (Exception e){
                    e.printStackTrace();
                }
            }
        });
        deleteUserButton.setBounds(350, 420, 150, 25);
        frame.getContentPane().add(deleteUserButton);


        final JButton backButton = new JButton("Back");
        backButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent arg0) {
                frame.dispose();
            }
        });
        backButton.setBounds(350, 480, 150, 25);
        frame.getContentPane().add(backButton);
    }

    private void createUIComponents() {
        // TODO: place custom component creation code here
    }
}