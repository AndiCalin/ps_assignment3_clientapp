package model;

public class Article {

	private int id;
	private String title;
	private String abstractOfArticle;
	private String body;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getAbstractOfArticle() {
		return abstractOfArticle;
	}
	public void setAbstractOfArticle(String abstractOfArticle) {
		this.abstractOfArticle = abstractOfArticle;
	}
	public String getBody() {
		return body;
	}
	public void setBody(String body) {
		this.body = body;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	
	@Override
	public String toString() {
		return "Article [id=" + id + ", title=" + title + ", abstractOfArticle=" + abstractOfArticle + ", body=" + body
				+ "]";
	}
}
