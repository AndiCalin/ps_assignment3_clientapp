package CommandCreator;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import entity.Article;
import entity.TransportObject;
import entity.User;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.SocketException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ArticleFunctionCaller {

    private ObjectMapper objectMapper;
    private CommandGenerator commandGenerator;

    public ArticleFunctionCaller() {
        objectMapper = new ObjectMapper();
        commandGenerator = new CommandGenerator();
    }

    public Article getArticleById(int id) {
        try {
            TransportObject transportObject = new TransportObject();
            transportObject.setCommandParameter("" + id);
            transportObject.setClassName("Article");
            transportObject.setCommandLine("SearchById");
            transportObject.setTransportObjectType(TransportObject.state.COMMAND);
            TransportObject response = commandGenerator.executeCommand(transportObject);
            if (response == null) {
                throw new RuntimeException("No article found!");
            } else {
                return objectMapper.readValue(response.getCommandBody(), Article.class);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public Article getArticleByTitle(String title) {
        try {
            TransportObject transportObject = new TransportObject();
            transportObject.setCommandParameter(title);
            transportObject.setClassName("Article");
            transportObject.setCommandLine("SearchByTitle");
            transportObject.setTransportObjectType(TransportObject.state.COMMAND);
            TransportObject response = commandGenerator.executeCommand(transportObject);
            if (response == null) {
                throw new RuntimeException("No article found!");
            } else {
                return objectMapper.readValue(response.getCommandBody(), Article.class);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public List<Article> getAllArticles(){
        try {
            TransportObject transportObject = new TransportObject();
            transportObject.setClassName("Article");
            transportObject.setCommandLine("ListAll");
            transportObject.setTransportObjectType(TransportObject.state.COMMAND);
            TransportObject response = commandGenerator.executeCommand(transportObject);
            if (response == null) {
                throw new RuntimeException("No article found!");
            } else {
                return new ArrayList<Article>(Arrays.asList(objectMapper.readValue(response.getCommandBody(), Article[].class)));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public List<Article> getAllArticlesForUser(User user){
        try {
            TransportObject transportObject = new TransportObject();
            transportObject.setClassName("Article");
            transportObject.setCommandLine("ListAllForUser");
            transportObject.setCommandParameter("" + user.getId());
            transportObject.setTransportObjectType(TransportObject.state.COMMAND);
            TransportObject response = commandGenerator.executeCommand(transportObject);
            if (response == null) {
                throw new RuntimeException("No article found!");
            } else {
                return new ArrayList<Article>(Arrays.asList(objectMapper.readValue(response.getCommandBody(), Article[].class)));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public Article insertArticle(Article article) throws JsonProcessingException {
        if (article.getBody().length() == 0){
            throw new RuntimeException("Please insert article text!");
        }
        TransportObject transportObject = new TransportObject();
        transportObject.setClassName("Article");
        transportObject.setCommandLine("InsertArticle");
        transportObject.setCommandBody(objectMapper.writeValueAsString(article));
        transportObject.setTransportObjectType(TransportObject.state.COMMAND);
        if (commandGenerator.executeCommand(transportObject).getCommandBody().equals("true")){
            return article;
        } else {
            return null;
        }
    }

    public Article updateArticle(Article article) throws JsonProcessingException {
        TransportObject transportObject = new TransportObject();
        transportObject.setClassName("Article");
        transportObject.setCommandLine("UpdateArticle");
        transportObject.setCommandBody(objectMapper.writeValueAsString(article));
        transportObject.setTransportObjectType(TransportObject.state.COMMAND);
        if (commandGenerator.executeCommand(transportObject).getCommandBody().equals("true")){
            return article;
        } else {
            return null;
        }
    }

    public void deleteArticle(int articleId){
        TransportObject transportObject = new TransportObject();
        transportObject.setClassName("Article");
        transportObject.setCommandLine("DeleteArticle");
        transportObject.setCommandParameter(""+articleId);
        transportObject.setTransportObjectType(TransportObject.state.COMMAND);
        commandGenerator.executeCommand(transportObject).getCommandBody();
    }

}
