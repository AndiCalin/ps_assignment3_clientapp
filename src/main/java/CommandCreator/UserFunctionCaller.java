package CommandCreator;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import entity.TransportObject;
import entity.User;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class UserFunctionCaller {

    private ObjectMapper objectMapper;
    private CommandGenerator commandGenerator;

    public UserFunctionCaller() {
        objectMapper = new ObjectMapper();
        commandGenerator = new CommandGenerator();
    }

    public User getUserById(int id) {
        try {
            TransportObject transportObject = new TransportObject();
            transportObject.setCommandParameter("" + id);
            transportObject.setClassName("User");
            transportObject.setCommandLine("SearchById");
            transportObject.setTransportObjectType(TransportObject.state.COMMAND);
            TransportObject response = commandGenerator.executeCommand(transportObject);
            if (response == null) {
                throw new RuntimeException("No user found!");
            } else {
                return objectMapper.readValue(response.getCommandBody(), User.class);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public User getUserByName(String name) {
        try {
            TransportObject transportObject = new TransportObject();
            transportObject.setCommandParameter(name);
            transportObject.setClassName("User");
            transportObject.setCommandLine("SearchByName");
            transportObject.setTransportObjectType(TransportObject.state.COMMAND);
            TransportObject response = commandGenerator.executeCommand(transportObject);
            if (response == null) {
                throw new RuntimeException("No user found!");
            } else {
                return objectMapper.readValue(response.getCommandBody(), User.class);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public List<User> getAllUsers() {
        try {
            TransportObject transportObject = new TransportObject();
            transportObject.setClassName("User");
            transportObject.setCommandLine("ListAll");
            transportObject.setTransportObjectType(TransportObject.state.COMMAND);
            TransportObject response = commandGenerator.executeCommand(transportObject);
            if (response == null) {
                throw new RuntimeException("No user found!");
            } else {
                return new ArrayList<User>(Arrays.asList(objectMapper.readValue(response.getCommandBody(), User[].class)));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public User insertUser(User user) throws JsonProcessingException {
        TransportObject transportObject = new TransportObject();
        transportObject.setClassName("User");
        transportObject.setCommandLine("InsertUser");
        transportObject.setCommandBody(objectMapper.writeValueAsString(user));
        transportObject.setTransportObjectType(TransportObject.state.COMMAND);
        if (commandGenerator.executeCommand(transportObject).getCommandBody().equals("true")){
            return user;
        } else {
            return null;
        }
    }

    public User updateUser(User user) throws JsonProcessingException {
        TransportObject transportObject = new TransportObject();
        transportObject.setClassName("User");
        transportObject.setCommandLine("UpdateUser");
        transportObject.setCommandBody(objectMapper.writeValueAsString(user));
        transportObject.setTransportObjectType(TransportObject.state.COMMAND);
        if (commandGenerator.executeCommand(transportObject).getCommandBody().equals("true")){
            return user;
        } else {
            return null;
        }
    }

    public void deleteUser(int userId){
        TransportObject transportObject = new TransportObject();
        transportObject.setClassName("User");
        transportObject.setCommandLine("DeleteUser");
        transportObject.setCommandParameter(""+userId);
        transportObject.setTransportObjectType(TransportObject.state.COMMAND);
        commandGenerator.executeCommand(transportObject);
    }

}