package CommandCreator;

import GUI.ClientMain;
import GUI.LoginWindow;
import com.fasterxml.jackson.databind.ObjectMapper;
import entity.TransportObject;

import java.io.BufferedReader;
import java.io.PrintWriter;

public class CommandGenerator {

    private ObjectMapper objectMapper;
    public BufferedReader input;
    public PrintWriter output;

    public CommandGenerator(){
        objectMapper = new ObjectMapper();
    }

    public TransportObject executeCommand(TransportObject transportObject){
        try {
            input = LoginWindow.input;
            output = LoginWindow.out;
            System.out.println(transportObject.toString());
            output.println(objectMapper.writeValueAsString(transportObject));
            while (true){
                String read = input.readLine();
                if (read != null){
                    System.out.println(read);
                    return objectMapper.readValue(read, TransportObject.class);
                }
            }
        } catch (Exception e) {
            return null;
        }
    }

}
