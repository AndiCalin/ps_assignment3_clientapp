package CommandCreator;

import com.fasterxml.jackson.databind.ObjectMapper;
import entity.TransportObject;

public class ExitCommandCaller {


    private ObjectMapper objectMapper;
    private CommandGenerator commandGenerator;

    public ExitCommandCaller() {
        objectMapper = new ObjectMapper();
        commandGenerator = new CommandGenerator();
    }

    public TransportObject ExitCommand() {
        try {
            TransportObject transportObject = new TransportObject();
            transportObject.setCommandLine("Exit");
            transportObject.setTransportObjectType(TransportObject.state.COMMAND);
            TransportObject response = commandGenerator.executeCommand(transportObject);
            return response;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

}
