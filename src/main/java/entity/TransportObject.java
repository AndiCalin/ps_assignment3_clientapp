package entity;

import org.json.JSONObject;

public class TransportObject {

    public enum state{COMMAND, RESPONSE};

    private state transportObjectType;
    private String className;
    private String commandLine;
    private String commandParameter;
    private String commandBody;

    public TransportObject(state transportObjectType, String className, String commandLine, String commandParameter, String commandBody) {
        this.transportObjectType = transportObjectType;
        this.className = className;
        this.commandLine = commandLine;
        this.commandParameter = commandParameter;
        this.commandBody = commandBody;
    }

    public TransportObject() {
    }

    public state getTransportObjectType() {
        return transportObjectType;
    }

    public void setTransportObjectType(state transportObjectType) {
        this.transportObjectType = transportObjectType;
    }

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    public String getCommandLine() {
        return commandLine;
    }

    public void setCommandLine(String commandLine) {
        this.commandLine = commandLine;
    }

    public String getCommandParameter() {
        return commandParameter;
    }

    public void setCommandParameter(String commandParameter) {
        this.commandParameter = commandParameter;
    }

    public String getCommandBody() {
        return commandBody;
    }

    public void setCommandBody(String commandBody) {
        this.commandBody = commandBody;
    }

    @Override
    public String toString() {
        return "TransportObject{" +
                "transportObjectType=" + transportObjectType +
                ", className='" + className + '\'' +
                ", commandLine='" + commandLine + '\'' +
                ", commandParameter='" + commandParameter + '\'' +
                ", commandBody='" + commandBody + '\'' +
                '}';
    }
}
